package pe.edu.cibertec.proyectobcp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pe.edu.cibertec.proyectobcp.model.CuentasBancarias;

@Repository
public interface CuentasBancariasRepository extends CrudRepository<CuentasBancarias, Long> {

	//public List<CuentasBancarias> findByCliente(Clientes cod_cli);

}
