package pe.edu.cibertec.proyectobcp.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import pe.edu.cibertec.proyectobcp.model.DestinoNotificaciones;
import pe.edu.cibertec.proyectobcp.model.Notificaciones;
import pe.edu.cibertec.proyectobcp.model.Transaccion;
import pe.edu.cibertec.proyectobcp.services.NotificacionesService;
import pe.edu.cibertec.proyectobcp.services.TransactionService;

@RestController
@RequestMapping("/api/transacciones")
public class TransactionRestController {

	@Autowired
	private NotificacionesService notificacionesService;
	@Autowired
	private TransactionService transactionService;

	@GetMapping("{codCuenta}")
	public List<Transaccion> obtenerTransacPorCuenta(@PathVariable Long codCuenta) {

		return transactionService.obtenerTransaccionesPorCuenta(codCuenta);
	}

	@PostMapping
	public ResponseEntity<Transaccion> crearTransaccion(@RequestBody Transaccion transaccion) {

		Transaccion transCreada = transactionService.registrar(transaccion);

		Notificaciones notificacion = new Notificaciones();

		DestinoNotificaciones destino = new DestinoNotificaciones();

		transactionService.registrarDestinoNotificacion(destino);

		notificacionesService.registrar(notificacion);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codCuenta}")
				.buildAndExpand(transCreada.getCod_cuenta()).toUri();

		return ResponseEntity.created(location).build();
	}

}
