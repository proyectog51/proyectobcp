package pe.edu.cibertec.proyectobcp.rest;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import pe.edu.cibertec.proyectobcp.model.Notificaciones;
import pe.edu.cibertec.proyectobcp.services.NotificacionesService;

@RestController
@RequestMapping("/api/notificaciones")
public class NotificacionesRestController {

	@Autowired
	private NotificacionesService notificacionesService;

	@GetMapping("{id}")
	public Notificaciones obtenerNotificacion(@PathVariable Long id) {

		return notificacionesService.obtenerNotificacion(id);
	}

	@PostMapping
	public ResponseEntity<Object> crearNotificacion(@RequestBody Notificaciones notificacion) {

		Notificaciones notifiCreada = notificacionesService.registrar(notificacion);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/id")
				.buildAndExpand(notifiCreada.getCod_notif()).toUri();

		return ResponseEntity.created(location).build();
	}

}
