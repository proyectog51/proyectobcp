package pe.edu.cibertec.proyectobcp.services;

import java.util.List;

import pe.edu.cibertec.proyectobcp.model.DestinoNotificaciones;
import pe.edu.cibertec.proyectobcp.model.Transaccion;

public interface TransactionService {

	Transaccion registrar(Transaccion transac);
	
	Transaccion obtenerTransaccion(Long codTran);
	
	List<Transaccion> obtenerTransaccionesPorCuenta(Long codCuenta);
	
	DestinoNotificaciones registrarDestinoNotificacion(DestinoNotificaciones destino);
	
}
