package pe.edu.cibertec.proyectobcp.services;

import java.util.List;

import pe.edu.cibertec.proyectobcp.model.Notificaciones;

public interface NotificacionesService {

	Notificaciones registrar(Notificaciones notificaciones);

	List<Notificaciones> obtenerNotifPorCuenta(Long codCuenta);
	
	Notificaciones obtenerNotificacion(Long codNotifi);

}
