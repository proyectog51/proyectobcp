package pe.edu.cibertec.proyectobcp.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.proyectobcp.model.DestinoNotificaciones;
import pe.edu.cibertec.proyectobcp.model.Transaccion;
import pe.edu.cibertec.proyectobcp.repository.DestinoNotificacionesRepository;
import pe.edu.cibertec.proyectobcp.repository.TransactionRepository;
import pe.edu.cibertec.proyectobcp.services.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;
	@Autowired
	private DestinoNotificacionesRepository destinoNotiRepository;

	@Override
	public Transaccion registrar(Transaccion transac) {

		return transactionRepository.save(transac);
	}

	@Override
	public Transaccion obtenerTransaccion(Long codTran) {

		return transactionRepository.findById(codTran).get();
	}

	@Override
	public List<Transaccion> obtenerTransaccionesPorCuenta(Long codCuenta) {
		List<Long> params = new ArrayList<Long>();

		params.add(codCuenta);

		return (List<Transaccion>) transactionRepository.findAllById(params);
	}

	@Override
	public DestinoNotificaciones registrarDestinoNotificacion(DestinoNotificaciones destino) {

		return destinoNotiRepository.save(destino);
	}

}
