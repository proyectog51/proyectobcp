package pe.edu.cibertec.proyectobcp.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.proyectobcp.model.Clientes;
import pe.edu.cibertec.proyectobcp.model.CuentasBancarias;
import pe.edu.cibertec.proyectobcp.repository.CuentasBancariasRepository;
import pe.edu.cibertec.proyectobcp.services.CuentasBancariasService;

@Service
public class CuentasBancariasServiceImpl implements CuentasBancariasService {

	@Autowired
	private CuentasBancariasRepository cuentasRepository;

	@Override
	public CuentasBancarias registrar(CuentasBancarias cuentas) {
		
		return cuentasRepository.save(cuentas);
	}

	@Override
	public Integer eliminar(Long id) {
		
		cuentasRepository.deleteById(id);
		Optional<CuentasBancarias> cta = cuentasRepository.findById(id);
		if (cta.isPresent()) {
			return -1;
		}
		return 1;
	}

	@Override
	public List<CuentasBancarias> obtenerCuentas(Clientes cliente) {
		Long id = cliente.getCod_cli();
		
		List<Long> params = new ArrayList<Long>();
		
		params.add(id);
		
		return (List<CuentasBancarias>) cuentasRepository.findAllById(params);
	}

}
