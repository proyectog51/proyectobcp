package pe.edu.cibertec.proyectobcp.services;

import java.util.List;

import pe.edu.cibertec.proyectobcp.model.Clientes;
import pe.edu.cibertec.proyectobcp.model.CuentasBancarias;

public interface CuentasBancariasService {

	CuentasBancarias registrar(CuentasBancarias cuentas);
	
//	CuentasBancarias actualizar(BigDecimal saldo);
	
	Integer eliminar(Long id);
	
	List<CuentasBancarias> obtenerCuentas(Clientes cliente);


}
