package pe.edu.cibertec.proyectobcp.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.cibertec.proyectobcp.model.Notificaciones;
import pe.edu.cibertec.proyectobcp.repository.NotificacionesRepository;
import pe.edu.cibertec.proyectobcp.services.NotificacionesService;

@Service
public class NotificacionesServiceImpl implements NotificacionesService {

	@Autowired
	private NotificacionesRepository notificacionesRepository;

	@Override
	public Notificaciones registrar(Notificaciones notificaciones) {

		return notificacionesRepository.save(notificaciones);
	}

	@Override
	public List<Notificaciones> obtenerNotifPorCuenta(Long codCuenta) {
		List<Long> params = new ArrayList<Long>();

		params.add(codCuenta);

		return (List<Notificaciones>) notificacionesRepository.findAllById(params);
	}

	@Override
	public Notificaciones obtenerNotificacion(Long codNotifi) {

		return notificacionesRepository.findById(codNotifi).get();
	}

}
